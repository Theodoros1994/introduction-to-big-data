# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 01:49:54 2017

@author: teo
"""

#import libraries

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
#describe of dataset can be done simply by reading the u.info file
f=open("C:\\Users\\teo\\Downloads\\ml-100k\\u.info" ,"r+")
stringg = f.read();
print (stringg)
# Close opend file
f.close()



#In order to print the different frequencies it's better to open file as a pandas dataframe


df_data=pd.read_csv("C:\\Users\\teo\\Downloads\\ml-100k\\u.data",names=['user_id','movie_id','rating','timestamp'],header=None,sep='\t')
df_items=pd.read_csv("C:\\Users\\teo\\Downloads\\ml-100k\\u.item",names=['movie_id','movie_title','release_date','videorealease_date','url','unkwonn','action','adventure','animation','children','comedy','crime','documentary','drama','fantasy','noir','horror','musical','mystery','romance','scifi','thriller','war','western'],header=None,sep='|',encoding="iso-8859-1")
df_users=pd.read_csv("C:\\Users\\teo\\Downloads\\ml-100k\\u.user",names=['user_id','age','gender','occupation','zipcode'],header=None,sep='|')





#print df_items
movies_merge=pd.merge(df_data,df_items)
users_merge=pd.merge(df_data,df_users)
top_tags=pd.merge(df_data,df_items)


df.groupby('Text').count()
# Apply the sum function to the groupby object 2415336255 me 233 

c=df.User_Id.value_counts()
users=df_data
#movies_merge['movie_id'].value_counts()[:25].plot()
#movies_merge.groupby('movie_title').size().sort_values(ascending=False)[:20].hist()
#PLOT OF MOST POPULAR BY RATING MOVIES
most_rated = movies_merge.groupby('movie_title').size().sort_values(ascending=False)[:25].plot(kind="barh")
plt.figure()
top_users=users_merge.groupby('zipcode').size().sort_values(ascending=False)[:25].plot(kind="barh")
plt.figure()
#top_tags=tags_merge.groupby('zipcode').size().sort_values(ascending=False)[:25].plot(kind="barh")
tagsDict={
  'Unknown':0,
  'Action':0 ,
  'Adventure':0 ,
  'Animation':0,'Children':0,
  'Comedy':0 ,
  'Crime':0 ,
  'Documentary':0 ,
  'Drama':0,
  'Fantasy':0 ,
  'noir':0 ,
  'Horror':0 ,
  'Musical':0 ,
  'Mystery':0 ,
  'Romance':0 ,
  'scifi':0 ,
  'Thriller':0 ,
  'War':0 ,'Western':0}

tagsDict['Unknown']+=top_tags['unkwonn'].sum()
tagsDict['Action']=top_tags['action'].sum()
tagsDict['Adventure']=top_tags['adventure'].sum()
tagsDict['Animation']=top_tags['animation'].sum()
tagsDict['Comedy']=top_tags['comedy'].sum()
tagsDict['Children']=top_tags['children'].sum()
tagsDict['Crime']=top_tags['crime'].sum()
tagsDict['Documentary']=top_tags['documentary'].sum()
tagsDict['Drama']=top_tags['drama'].sum()
tagsDict['Fantasy']=top_tags['fantasy'].sum()
tagsDict['noir']=top_tags['noir'].sum()
tagsDict['Horror']=top_tags['horror'].sum()
tagsDict['Musical']=top_tags['musical'].sum()
tagsDict['Mystery']=top_tags['mystery'].sum()
tagsDict['Romance']=top_tags['romance'].sum()
tagsDict['scifi']=top_tags['scifi'].sum()
tagsDict['Thriller']=top_tags['thriller'].sum()
tagsDict['War']=top_tags['war'].sum()
tagsDict['Western']=top_tags['western'].sum()

from collections import OrderedDict
Tag_sorted = OrderedDict(sorted(tagsDict.items(), key=lambda x: x[1]))

#plt.hist(list(tagsDict.keys()), tagsDict.values())

plt.bar(range(len(Tag_sorted)), Tag_sorted.values(), align="center")

plt.xticks(range(len(Tag_sorted)), Tag_sorted.keys(), rotation=55)
#plt.xticks(range(len(d)), [split_word(i) for i in d.keys()])
#plt.gca().invert_xaxis()
#plt.gca().invert_yaxis()
#print most_rated
#plt.hist(most_rated,rotation='vertical')
#plt.style.use(['dark_background', 'presentation'])
#plt.hist(most_rated, bins=np.arange(len(most_rated).min(), len(most_rated).max()+1))
#plt.style.use('ggplot')
plt.title('Movies Tag Frequency Graph')
plt.ylabel('Freq')
plt.xlabel('Movie Tags')
plt.gcf().subplots_adjust(bottom=0.15)
plt.show()
#plt.legend()

#FOR OUTLIERS THAT I DIDNT HAVE TIME I WOULD MAKE THE Tag_sorted dataframe to a list to pass it on the outliers function
# Same for the users who used tags, i would create a new columns with the sum of tags used per user 1 for horror+0+1 for adventure etc and pass it into function also

#def reject_outliers(data):
 #   u = np.median(data)
  #  s = np.std(data)
   # filtered = [e for e in data if (u - 2 * s < e < u + 2 * s)]
    #return filtered

# a second approach would be to use IQR method whic outlies whatever is under 25% and over 75% of the plot, in this variation we should be careful with the numerical data to be in proper form for the function
	#def outliers_iqr(ys):
    #quartile_1, quartile_3 = np.percentile(ys, [25, 75])
    #iqr = quartile_3 - quartile_1
    #lower_bound = quartile_1 - (iqr * 1.5)
    #upper_bound = quartile_3 + (iqr * 1.5)
    #return np.where((ys > upper_bound) | (ys < lower_bound))
#filtered = reject_outliers()

#plt.hist(filtered, 50)
#plt.show()
n_users = movies_merge.user_id.nunique()
n_items = movies_merge.movie_id.nunique()

#print('Num. of Users: '+ str(n_users))
#print('Num of Movies: '+str(n_items))
from sklearn.cross_validation import train_test_split
train_data, test_data = train_test_split(movies_merge, test_size=0.25)


#Create two user-item matrices, one for training and another for testing
train_data_matrix = np.zeros((n_users, n_items))
for line in train_data.itertuples():
    train_data_matrix[line[1]-1, line[2]-1] = line[3]  

test_data_matrix = np.zeros((n_users, n_items))
for line in test_data.itertuples():
    test_data_matrix[line[1]-1, line[2]-1] = line[3]
    
    
from sklearn.metrics.pairwise import pairwise_distances
user_similarity = pairwise_distances(train_data_matrix, metric='cosine')



def predict(ratings, similarity, type='user'):
    if type == 'user':
        mean_user_rating = ratings.mean(axis=1)
        #You use np.newaxis so that mean_user_rating has same format as ratings
        ratings_diff = (ratings - mean_user_rating[:, np.newaxis]) 
        pred = mean_user_rating[:, np.newaxis] + similarity.dot(ratings_diff) / np.array([np.abs(similarity).sum(axis=1)]).T
    return pred



user_prediction = predict(train_data_matrix, user_similarity, type='user')





from sklearn.metrics import mean_squared_error
from math import sqrt
def rmse(prediction, ground_truth):
    prediction = prediction[ground_truth.nonzero()].flatten() 
    ground_truth = ground_truth[ground_truth.nonzero()].flatten()
    return sqrt(mean_squared_error(prediction, ground_truth))



print('User-based CF RMSE: ' + str(rmse(user_prediction, test_data_matrix)))

#For the recommender part 2 i would use a table created by assigned weights for different factors like age to multiply it with rating_diff in my predict function
# The table could be generated with many if statements, if user==A (A may refer to 20-25 group) then total weight=0*33*age+total weight else total weight=total weight
#More briefly total weight=Wage+Wzipcode etc where its weight can be 0,33 if they match or 0 if they dont

